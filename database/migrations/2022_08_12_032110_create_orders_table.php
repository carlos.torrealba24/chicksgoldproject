<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->double('total')->nullable();
            $table->integer('payment_method_id');
            $table->string('payment_method_name',100)->nullable();
            $table->integer('account_id')->unsigned();
            $table->enum('status', ['1', '0']);
            //$table->softDeletes($column = 'deleted_at', $precision = 0);
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
