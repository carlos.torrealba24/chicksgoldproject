<?php

namespace Database\Seeders;

use App\Models\Categories;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categories::Create([
            'description' => 'League of Legends',
        ]);
        Categories::Create([
            'description' => 'OSR3',
        ]);
        Categories::Create([
            'description' => 'RS3',
        ]);
        Categories::Create([
            'description' => 'Lost Ark',
        ]);
        Categories::Create([
            'description' => 'Final Fantasy 14',
        ]);
        Categories::Create([
            'description' => 'Path of Exile',
        ]);
        Categories::Create([
            'description' => 'Escape from Tarkov',
        ]);
        Categories::Create([
            'description' => 'New World',
        ]);
        Categories::Create([
            'description' => 'WoW Classic',
        ]);
        Categories::Create([
            'description' => 'WoW TBC',
        ]);
        Categories::Create([
            'description' => 'WoW SoM',
        ]);
        Categories::Create([
            'description' => 'WoW Retail',
        ]);
    }
}
