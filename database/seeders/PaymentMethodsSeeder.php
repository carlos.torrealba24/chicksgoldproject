<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::Create([
            'description' => 'Paypal',
        ]);
        PaymentMethod::Create([
            'description' => 'Btc',
        ]);
        PaymentMethod::Create([
            'description' => 'Ethereum',
        ]);
        PaymentMethod::Create([
            'description' => 'Credit Card',
        ]);
        PaymentMethod::Create([
            'description' => 'Skrill',
        ]);
    }
}
