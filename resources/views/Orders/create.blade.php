@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <form action="{{route('orders.register', $account)}}" method="post">
                @csrf
                <div class="form-group mb-3">
                        <div class="form-group mb-3">
                            <label for="">Category:</label>
                            <input type="text" name="category" id="category" class="form-control" placeholder="Type a category" aria-describedby="helpId" value="{{$account->category_name}}" disabled>
                        </div>
                        
                        <div class="form-group mb-3">   
                            <label for="">Title:</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="Type a title" aria-describedby="helpId" value="{{$account->title}}" disabled>
                        </div>
                        <div class="form-group mb-3">   
                            <label for="">Price:</label>
                            <input type="text" name="price" id="price" class="form-control" placeholder="Type a price" aria-describedby="helpId" value="{{$account->price}}" disabled>
                        </div>
                        
                        <div class="form-group mb-3">
                            <label for="">Description:</label>
                            <input type="text" name="description" id="description" class="form-control" placeholder="Type a description" aria-describedby="helpId" value="{{$account->description}}" disabled>
                        </div>

                        <div class="form-group mb-3">
                            <label for="">PayMethod:</label>
                            <select class="form-control"style="width: 200px;" name="paymentMethod" id="paymentMethod" required>
                                <option>Select a payment method</option>
                                @foreach ($payment as $pay)
                                    <option value="{{$pay->id}}">{{$pay->description}}</option>
                                @endforeach
                            </select>
                        </div>
                </div>
                
                <div class="float-right">
                    <button class="btn btn-success" type="submit" id="createOrder">Create Order</button>
                </div>
            </form>
        </div>
    </div>    
@endsection
