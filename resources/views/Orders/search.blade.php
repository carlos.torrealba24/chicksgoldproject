@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-5" style="display:flex;">
            <div class="col-md-6">
                <h3 class="text-secondary">Orders</h3>
            </div>
            <div class="col-md-6">
                <div style="display: flex;">
                    <form action="{{route('orders.search')}}" method="get" style="display: flex">
                        <input class="form-control" style="width: 200px;" type="search" name="search" id="search" placeholder="Type a word to search">
                        <button class="btn btn-success"type="submit">Go</button>
                        
                    </form>
                    <form action="{{route('orders.index')}}" method="get">
                        <button class="btn btn-primary"type="submit">Reset</button>
                    </form>
            </div>
        </div>
    </div>
        <div class="col-md-12">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Total</th>
                        <th>Payment Method</th>
                        <th>Account ID</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orderFinded as $ordFinded)
                    <tr>
                        <td>{{$ordFinded->id}}</td>
                        <td>{{$ordFinded->total}}</td>
                        <td>{{$ordFinded->payment_method_name}}</td>
                        <td>{{$ordFinded->account_id}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-md-12">
                <form action="{{route('accounts.index')}}" method="get">
                    <button class="btn btn-warning" type="submit">Back</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
