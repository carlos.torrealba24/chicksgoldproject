@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <form action="{{route('accounts.actualizar', $account)}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <div class="form-group mb-3">
                        <label for="">Category:</label>
                        <input type="text" name="category" id="category" class="form-control" placeholder="Type a category" aria-describedby="helpId" value="{{$account->category}}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Title:</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Type a title" aria-describedby="helpId" value="{{$account->title}}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Price:</label>
                        <input type="text" name="price" id="price" class="form-control" placeholder="Type a price" aria-describedby="helpId" value="{{$account->price}}">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Description:</label>
                        <input type="text" name="description" id="description" class="form-control" placeholder="Type a description" aria-describedby="helpId" value="{{$account->description}}">
                    </div>
                </div>
                
                <div class="float-right">
                    <button class="btn btn-primary" type="submit">Edit</button>
                </div>
            </form>
        </div>
    </div>    
@endsection