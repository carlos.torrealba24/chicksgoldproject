@extends('layouts.app')

@section('content')
        <div class="container">
            <div class="row mb-5" style="display:flex;">
                <div class="col-md-6 mb-5">
                    <a type="button" class ="btn btn-primary" href="{{route('accounts.create')}}">Create new category</a>
                </div>
                <div class="col-md-6">
                    <div style="display: flex;">
                        <form action="{{route('accounts.search')}}" method="get" style="display: flex">
                            <input class="form-control" style="width: 200px;" type="search" name="search" id="search" placeholder="Type a word to search">
                            <button class="btn btn-success"type="submit">Go</button>
                            
                        </form>
                        <form action="{{route('accounts.index')}}" method="get">
                            <button class="btn btn-primary"type="submit">Reset</button>
                        </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-light">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($accountFinded as $accFinded)
                    <tr>
                        <td>{{$accFinded->id}}</td>
                        <td>{{$accFinded->category_name}}</td>
                        <td>{{$accFinded->title}}</td>
                        <td>{{$accFinded->price}}</td>
                        <td>{{$accFinded->description}}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('orders.create', $accFinded->id) }}"
                                    class="btn btn-success" title="Edit">Buy
                                </a>
                                <a href="{{ route('accounts.edit', $accFinded->id) }}"
                                    class="btn btn-warning" title="Edit">Edit
                                </a>
                                <form action="{{url('accounts/delete/'.$accFinded->id)}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="col-md-12">
                <form action="{{route('accounts.index')}}" method="get">
                    <button class="btn btn-warning" type="submit">Back</button>
                </form>
            </div>
        </div>
    </div>
@endsection
