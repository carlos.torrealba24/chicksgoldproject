@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <form action="{{route('accounts.register')}}" method="post">
                @csrf
                <div class="form-group mb-3">
                    <div class="form-group mb-3">
                        <label for="">Category:</label>
                        <select class="form-control"style="width: 200px;" name="categoriesFilter" id="categoriesFilter">
                            <option value="0">Select a category</option>
                            @foreach ($categories as $categ)
                                <option value="{{$categ->id}}">{{$categ->description}}</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Title:</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Type a title" aria-describedby="helpId">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Price:</label>
                        <input type="text" name="price" id="price" class="form-control" placeholder="Type a price" aria-describedby="helpId">
                    </div>
                    <div class="form-group mb-3">
                        <label for="">Description:</label>
                        <input type="text" name="description" id="description" class="form-control" placeholder="Type a description" aria-describedby="helpId">
                    </div>   
                </div>
                
                <div class="float-right">
                    <button class="btn btn-success" type="submit">Registrar</button>
                </div>
            </form>
        </div>
    </div>    
@endsection
