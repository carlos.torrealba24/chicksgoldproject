<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Accounts extends Model
{
    //use SoftDeletes;
    use HasFactory;
    protected $table = "accounts";

    // datos
    protected $fillable = [
        'id',
        'description',
        'status',
        'title',
        'category_id',
        'price'
    ];
    protected $timestamp = false;


    // Relaciones

    public function orders()
    {
        return $this->hasMany(Orders::class);
    }

}