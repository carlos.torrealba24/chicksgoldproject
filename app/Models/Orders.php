<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Orders extends Model
{
    use HasFactory;
    //use SoftDeletes;

    // nombre de la tabla que usa el modelo
    protected $table = "orders";

    // datos
    protected $fillable = [
        'payment_method',
        'status',
        'total',
        'account_id'
    ];

    protected $timestamp = false;

    // relaciones
    public function accounts()
    {
        return $this->belongsTo(Accounts::class);
    }
}
