<?php

namespace App\Http\Controllers;

use App\Models\Accounts;
use App\Models\Categories;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    public function index(){
        $accounts = Accounts::paginate(10)->where('status', '=', '1');
        $categories = Categories::all();
        return view('accounts.index', compact('accounts', 'categories'));
    }

    public function create(){
        $categories = Categories::all();
        return view('accounts.create', compact('categories'));
    }

    public function register(Request $request){
        $categories = Categories::where('id', '=', $request->categoriesFilter)->get()->first();
        $account = new Accounts();
        $account->category_id = $request->categoriesFilter;
        $account->category_name = $categories->description;
        $account->title = $request->title;
        $account->price = $request->price;
        $account->description = $request->description;
        $account->status = '1';
        $account->save();
        return redirect()->route('accounts.index');
    }

    public function edit($acc){
        $account = Accounts::where('id', '=', $acc)->get()->first();
        return view('accounts.edit', compact('account'));
    }

    public function actualizar(Request $request, Accounts $acc){
        $acc->category = $request->category;
        $acc->title = $request->title;
        $acc->price = $request->price;
        $acc->description = $request->description;
        $acc->save();
        return redirect()->route('accounts.index');
    }

    public function delete($acc){
        $accToDelete = Accounts::where('id', '=', $acc)->get()->first();
        $accToDelete->status = '0';
        $accToDelete->save();
        return redirect()->route('accounts.index');
    }

    public function search(){
        $search = $_GET['search'];
        $accounts = Accounts::paginate(10)->where('status', '=', '1');
        $categories = Categories::all();
        $accountFinded = Accounts::where('category_name', 'LIKE', '%'.$search.'%')->orWhere('title', 'LIKE', '%'.$search.'%')->orWhere('description', 'LIKE', '%'.$search.'%')->get();
        return view('accounts.search', compact('accounts', 'categories', 'accountFinded'));
    }

}
