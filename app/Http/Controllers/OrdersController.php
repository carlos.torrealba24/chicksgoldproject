<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\Accounts;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;

class OrdersController extends Controller
{

    public function index(){
        $orders = Orders::paginate(10)->where('status', '=', '1');
        $payment = PaymentMethod::all();
        return view('orders.index', compact('orders', 'payment'));
    }

    public function create($acc){
        $payment = PaymentMethod::all();
        $account = Accounts::where('id', '=', $acc)->get()->first();
        return view('orders.create', compact('account', 'payment'));
    }

    public function register(Request $request, Orders $orders, $acc){
        $account = Accounts::where('id', '=', $acc)->get()->first();
        $payment = PaymentMethod::where('id', '=', $request->paymentMethod)->get()->first();
        $orders = new Orders();
        if(!$request->paymentMethod == 0){
            $orders->total = $account->price;
            $orders->payment_method_id = $request->paymentMethod;
            $orders->payment_method_name = $payment->description;
            $orders->account_id = $account->id;
            $account->status = '0';
            $account->save();
            $orders->save();
        }else{
            return back();
        }
        return redirect()->route('orders.index');
    }

    public function search(){
        $search = $_GET['search'];
        $orders = Orders::paginate(10)->where('status', '=', '1');
        $payment = PaymentMethod::all();
        $orderFinded = Orders::where('payment_method_name', 'LIKE', '%'.$search.'%')->orWhere('account_id','=', $search)->get();
        return view('orders.search', compact('orders', 'payment', 'orderFinded'));
    }
}
