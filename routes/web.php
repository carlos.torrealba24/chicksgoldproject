<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\AccountsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// rutas accounts
Route::get('/accounts/index', [AccountsController::class, 'index'])->name('accounts.index')->middleware('auth')->middleware('auth');
Route::get('/accounts/create', [AccountsController::class, 'create'])->name('accounts.create')->middleware('auth');
Route::get('/accounts/edit/{acc}', [AccountsController::class, 'edit'])->name('accounts.edit')->middleware('auth');
Route::put('/accounts/actualizar/{acc}', [AccountsController::class, 'actualizar'])->name('accounts.actualizar')->middleware('auth');
Route::delete('/accounts/delete/{acc}', [AccountsController::class, 'delete'])->name('accounts.delete')->middleware('auth');
Route::post('/accounts/register', [AccountsController::class, 'register'])->name('accounts.register')->middleware('auth');
Route::get('/accounts/search', [AccountsController::class, 'search'])->name('accounts.search');


// rutas orders
Route::get('/orders/index', [OrdersController::class, 'index'])->name('orders.index')->middleware('auth');
Route::get('/orders/create/{id}', [OrdersController::class, 'create'])->name('orders.create')->middleware('auth');
Route::get('/orders/edit/{id}', [OrdersController::class, 'edit'])->name('orders.edit')->middleware('auth');
Route::put('/orders/edit/{id}', [OrdersController::class, 'actualizar'])->name('orders.edit')->middleware('auth');
Route::delete('/orders/delete/{id}', [OrdersController::class, 'delete'])->name('orders.delete')->middleware('auth');
Route::post('/orders/register/{account}', [OrdersController::class, 'register'])->name('orders.register')->middleware('auth');
Route::get('/orders/search', [OrdersController::class, 'search'])->name('orders.search');

Auth::routes();

Route::get('/home', [AccountsController::class, 'index'])->name('accounts.index')->middleware('auth');

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('/', [AccountsController::class, 'index'])->name('home');
});
